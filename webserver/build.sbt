import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.Keys._
import NativePackagerKeys._
import com.typesafe.sbt.packager.archetypes.ServerLoader.{SystemV, Upstart}

scalaVersion := "2.11.7"

lazy val commons = ProjectRef(file("../commons"), "commons")

lazy val root = (project in file(".")).enablePlugins(PlayJava).dependsOn(commons)

serverLoading in Debian := Upstart

name := "valooto"

version := "2.8.0" 

organization := "com.valooto"

maintainer in Debian := "Alon Lubin"

packageSummary in Debian := "Valooto Production"

packageDescription in Debian := "Valooto Production"

lazy val scalacheck = "org.scalacheck" %% "scalacheck" % "1.12.5"

sourceDirectory in Test <<= baseDirectory(_ / "test")

resourceDirectory in Test <<= baseDirectory(_ / "test" / "resources") 

dependencyOverrides += "org.mockito" % "mockito-core" % "1.10.8"

dependencyOverrides += "commons-logging" % "commons-logging" % "1.2"

dependencyOverrides += "org.hamcrest" % "java-hamcrest" % "2.0.0.0"

dependencyOverrides += "junit" % "junit" % "4.12" //make sure all libs use latest junit version

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  filters,
  "com.novocode" % "junit-interface" % "0.11" % "test",
  "org.hamcrest" % "java-hamcrest" % "2.0.0.0" % "test",
  "com.github.mumoshu" %% "play2-memcached" % "0.6.0",  
  "be.objectify" %% "deadbolt-java" % "2.3.3",
  "mysql" % "mysql-connector-java" % "5.1.30",
  "javax.mail" % "mail" % "1.4.7",
  "com.h2database" % "h2" % "1.4.191",
  "com.google.inject" % "guice" % "3.0",
  "com.tzavellas" % "sse-guice" % "0.7.1",
  "aopalliance" % "aopalliance" % "1.0",
  "com.force.api" % "force-metadata-api" % "34.0.0",
  "com.force.api" % "force-partner-api" % "34.0.0",
  "com.force.api" % "force-wsc" % "34.2.0",
  "org.apache.poi" % "poi" % "3.14",
  "org.apache.servicemix.bundles" % "org.apache.servicemix.bundles.poi" % "3.14_1",
  "org.mockito" % "mockito-all" % "1.9.5",
  "commons-lang" % "commons-lang" % "2.6",
  "net.sf.jtidy" % "jtidy" % "r938",
  "org.xhtmlrenderer" % "core-renderer" % "R8pre2",
  "commons-io" % "commons-io" % "2.4",
  "com.lowagie" % "itext" % "2.1.7",
  "com.itextpdf" % "itextpdf" % "5.5.9",
  "com.itextpdf.tool" % "xmlworker" % "5.5.9",
  "org.xhtmlrenderer" % "flying-saucer-parent" % "9.0.6",
  "org.docx4j" % "docx4j" % "3.3.0",
  "org.docx4j" % "docx4j-export-fo" % "3.3.0",
  "org.docx4j" % "docx4j-ImportXHTML" % "3.3.0",
  "org.openoffice" % "unoil" % "3.2.1",
  "org.openoffice" % "juh" % "3.2.1",
  "com.artofsolving" % "jodconverter" % "2.2.1",
  "com.github.livesense" % "jodconverter-core" % "1.0.5",
  "fr.opensagres.xdocreport" % "org.apache.poi.xwpf.converter.pdf" % "1.0.5",
  "fr.opensagres.xdocreport" % "fr.opensagres.xdocreport.document" % "1.0.5",
  "fr.opensagres.xdocreport" % "fr.opensagres.xdocreport.document.docx" % "1.0.5",
  "fr.opensagres.xdocreport" % "fr.opensagres.xdocreport.template.velocity" % "1.0.5",
  "fr.opensagres.xdocreport" % "fr.opensagres.xdocreport.converter.docx.xwpf" % "1.0.5",
  "fr.opensagres.xdocreport" % "fr.opensagres.xdocreport.converter.docx.docx4j" % "1.0.5",
  "mysql" % "mysql-connector-mxj" % "5.0.12" % "test",
  "org.freemarker" % "freemarker" % "2.3.14",
  "org.mockito" % "mockito-core" % "1.10.8" exclude("org.hamcrest" , "hamcrest-core"), // for unit test. removed dependency on old hamcrest
  "org.apache.jclouds.api" % "openstack-nova" % "1.8.1",
  "org.apache.jclouds.driver" % "jclouds-slf4j" % "1.8.1",
  "org.apache.jclouds.driver" % "jclouds-sshj" % "1.8.1",
  "org.apache.jclouds" % "jclouds-all" % "1.8.1",
  "org.apache.jclouds.labs" % "rackspace-cloudfiles-us" % "1.8.1",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "com.aspose" % "aspose-cloud-sdk" % "1.0",
  "com.aspose" % "aspose-cloud-words" % "1.0.0",
  "com.sun.jersey" % "jersey-core" % "1.19", // required by aspose
  "commons-beanutils" % "commons-beanutils" % "1.9.2",
  "commons-logging" % "commons-logging" % "1.2",
  "commons-collections" % "commons-collections" % "3.2.1",
  "net.sf.uadetector" % "uadetector-resources" % "2014.10",
  "mysql" % "mysql-connector-mxj-db-files" % "5.0.12",
  "com.esotericsoftware.kryo" % "kryo" % "2.24.0",
  "net.htmlparser.jericho" % "jericho-html" % "3.3",
  "net.sf.opencsv" % "opencsv" % "2.3",
  "org.apache.velocity" % "velocity" % "1.7",
  "joda-time" % "joda-time" % "2.8.1",
  "org.ff4j" % "ff4j-core" % "1.3.5",
  "commons-validator" % "commons-validator" % "1.5.0",
  "com.tngtech.java" %  "junit-dataprovider" % "1.11.0" % "test" , // Provides NG-Test like data providers for unit tests
  "junit" % "junit" % "4.12" % "test" excludeAll(ExclusionRule(organization = "org.hamcrest")), // Removed dependency on old hamcrest
  "com.googlecode.junit-toolbox" % "junit-toolbox" % "1.10" % "test" excludeAll(ExclusionRule(organization = "org.hamcrest"))//Java 1.6. 2.2 is for Java 8
)

resolvers += Resolver.url("Objectify Play Repository", url("http://schaloner.github.io/releases/"))(Resolver.ivyStylePatterns)

resolvers += Resolver.url("Objectify Play Repository - snapshots", url("http://schaloner.github.io/snapshots/"))(Resolver.ivyStylePatterns)

resolvers += "Typesafe Releases" at "http://typesafe.artifactoryonline.compesafe"

resolvers += "pk11 repo" at "http://pk11-scratch.googlecode.com/svn/trunk/"

resolvers += "Spy Repository" at "http://files.couchbase.com/maven2" // required to resolve `spymemcached`, the plugin's dependency.

resolvers += "Aspose Java API" at "http://maven.aspose.com/artifactory/simple/ext-release-local" // for aspose Java library

(testOptions in Test) += Tests.Argument(TestFrameworks.Specs2, "html")

jacoco.settings

parallelExecution      in jacoco.Config := false

jacoco.outputDirectory in jacoco.Config := file("target/jacoco")


# !/usr/bin/python
import datetime
import os
import re

debug = True

def get_active_accounts():
    print "+getting active accounts from db...\n"

    sql="select cfas_cfac_id, cfas_company_name from configuration.cfas_account_settings where cfas_is_sandbox = 0\\G"
    cmd='mysql -uroot -proot -e \"%s\"' % sql

    output = os.popen(cmd).read()


    accounts={}
    key = ''
    for line in output.split('\n'):

        matchObj=re.match(r"\s+cfas_cfac_id: (\d+)", line, re.I)
        if matchObj:
            key = matchObj.group(1)
            continue

        matchObj=re.match(r"cfas_company_name: (.*)", line, re.I)
        if matchObj:
            val= matchObj.group(1)
            accounts[key]=val
            key=''
            val=''

    return accounts
def find_unassigned_quotes(id):

    sql="SELECT * FROM configuration.uvqu_user_quotes where uvqu_cfac_id = %s  AND uvqu_status = 'ApprovalProcess'\
    and uvqu_guid not in (SELECT distinct wfpa_uvqu_guid FROM configuration.wfpa_workflow_proposal_approvals where wfpa_cfac_id = %s AND wfpa_status = 'WAITING')\
    order by uvqu_updated desc\\G" % (id,id)

    cmd='mysql -uroot -proot -e \"%s\"' % sql
    
    output = os.popen(cmd).read()

    return output

def find_relevant(timeFrom,output):
    # gets time in format: YYYY-MM-DD HH:mm:ss
    #2016-04-14 06:59:30 
    #
    debug = True
    
    if debug is True:
        fh=open('out.txt','r+')        
        if fh:
            output=fh.read()
    
    for line in output.split("\n"):
        line="uvqu_updated: 2015-12-10 13:24:37"
        print line
        matchObj=re.match(r"uvqu_updated:\s+(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)", line, re.I)
        if matchObj:
            year=matchObj.group(1)
            month=matchObj.group(2)
            day=matchObj.group(3)
            hour=matchObj.group(4)
            min=matchObj.group(5)
            sec=matchObj.group(6)
        #only show entries if a quotes was updated since timeFrom
        
    return output

def get_time(time_string):
     matchObj=re.match(r"(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)", time_string, re.I)
        if matchObj:
            time={
                    'year' :    matchObj.group(1)
                    'month':    matchObj.group(2)
                    'day'  :    matchObj.group(3)
                    'hour' :    matchObj.group(4)
                    'min'  :    matchObj.group(5)
                    'sec'  :    matchObj.group(6)
                    }
        else:
            print unable to parse time format. exiting...
            quit()
                    
    return time

###
timeFrom = "2016-06-01 00:00:00"

print str(datetime.datetime.now().date())+" "+str(datetime.datetime.now().time())+" Script Started\n"
accounts=get_active_accounts()
print accounts
output=''
for key in accounts:
    output=output+ "Account: %s \n" % accounts[key]
    output=output+find_unassigned_quotes(key)+"\n\n"

print output
print find_relevant(timeFrom,output)

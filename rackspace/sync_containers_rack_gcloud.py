#!/usr/bin/env python
"""
Package sync_containers_from_rack_gcloud
Author: Omri Levi
Version: 1.0

This package logs in to Rackspace,
and copies the contents to a local directory. 

"""

import argparse
import pyrax
import sys
import os
import platform
import logging
import errno
import re   
import pprint

debug_mode=True
#print debug_mode
logging.basicConfig(filename='files_rackspce.txt',level=logging.DEBUG)
logging.info("Starting script: sync_containers_from_rack_gcloud")
logging.info("debug mode: %s" % debug_mode)

def is_windows():
    os_type=platform.platform()

    if "windows" in os_type.lower():
        return True
    else:
        return False

def get_filename(string):
    matchObj=re.match(r"(.*)(')(.*)(')", string, re.I)
    print matchObj.group(3)
    return ""
    

#parse arguments
parser = argparse.ArgumentParser(description='sync container from Rackspace to local FS')
parser.add_argument('source', type=str ,help='source containers in Rackspace')
parser.add_argument('destination', type=str , help='destination container in google cloud')
parser.add_argument('-v', action='store_const', help='verify file integrity (checksum)' ,
                    const='store_true',)

args = parser.parse_args()
logging.info(args)

#calc path

if (is_windows()):
    root="D:\\rackspace\\"
else:
    root='/home/valooto/'

path=root+args.source

logging.info ("Destination path is:%s" % path)

#verify destination exists:
if not os.path.isdir(root):
    logging.critical ("destination root doesn't exist, exiting")
    exit()

if os.path.isdir(path):
    logging.info ("destination path exists")
else:
    try:
        logging.warn("path doesn't exist, creating path")
        os.mkdir(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

#login parameters:
##Rackspace
rack_user='valooto'
rack_key='833b33d45ae04b7f82ecbd0cbfe2483b'

#login Rackspace
logging.info ('loggin to Rackspace...')
try:
    pyrax.set_setting("identity_type", "rackspace")
    pyrax.set_default_region('DFW')
    response=pyrax.set_credentials(rack_user, rack_key)
except:
    e = sys.exc_info()[0]
    logging.error ("Error: %s" % e)

#Cloud file object
cf = pyrax.cloudfiles

#load container from source (Rackspace)
logging.info ('Source container:%s' % args.source)
source_cont= cf.get_container(args.source)
source_objs=source_cont.get_objects()


#cycle through files and download
count=0  #count downloaded files.

for obj in source_objs:
    pprint.pprint (vars(obj))
    if not 'application/directory' in obj.content_type:
        
        logging.info ('Handling file:%s'  % obj)
        logging.debug("Destination path is: %s" %path+"/"+obj.name)
    #print 'filename is:%s' %fn       
        if os.path.isfile(path+"/"+obj.name):
            logging.warn("File exists in path: %s - SKIPPING FILE" % path+"/"+obj.name)
            continue
        try:
            logging.info("Downloading file: %s " % obj.name)
            obj.download(path)
            count+=1
        
        except IOError as e:
            print "exception caught: %s" %e
            print "hold for debug"
        
        except WindowsError as w:
                print "exception caught: %s" %w
    else: #object is a directory. 
        if not os.path.isdir(path+"/"+obj.name):
            os.mkdir(path+"/"+obj.name)
        else:
            logging.info("directory: %s exists" % path+"/"+obj.name)
    fn=None
    #if (count > 4): break

summary= '\nSummary:\n'+'========\n' +'Source container:%s\n' % args.source + \
    'Destination container:%s\n' % path + 'Downloaded files count:%i\n' %count

print summary
logging.info(summary)
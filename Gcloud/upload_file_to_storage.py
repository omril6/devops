from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
from gcloud import datastore
from apiclient.http import MediaIoBaseUpload
from apiclient.http import MediaFileUpload


import argparse
import pprint
import logging
import os
import json

logging.basicConfig(filename='gcloud-storage-log.txt',level=logging.DEBUG)
logging.info("Starting script: upload files to gcloud")


def create_service():
    """Creates the service object for calling the Cloud Storage API."""
    # Get the application default credentials. When running locally, these are
    # available after running `gcloud init`. When running on compute
    # engine, these are available from the environment.
    credentials = GoogleCredentials.get_application_default()

    # Construct the service object for interacting with the Cloud Storage API -
    # the 'storage' service, at version 'v1'.
    # You can browse other available api services and versions here:
    #     https://developers.google.com/api-client-library/python/apis/
    return discovery.build('storage', 'v1', credentials=credentials)

def print_bucket(): 
    fields_to_return = 'nextPageToken,items(name,size,contentType)'
    req = service.objects().list(bucket=bucketName, fields=fields_to_return)
    if req is not None:
        resp = req.execute()
        items = resp["items"]
    for item in items:
        logging.info (item['name'])
        
def load_files(path):
    all_files={}
    dir_list=os.listdir(path)
    for fn in dir_list:
        if (os.path.isfile(path+fn)):
            aFile=os.stat(path+fn)
            all_files[path+fn]=aFile.st_size
    return all_files

def load_dirs(path):
    dir_list=[]

    for dirname, dirnames, filenames in os.walk(path):
        for subdirname in dirnames:
            dir_list.append(subdirname)
    return dir_list

def is_bucket(bucket):
    req = service.buckets().get(bucket=bucket)
    try:
        req.execute()
    except discovery.HttpError as e:
        if (e.resp['status'] == '400' or '404'):
            return False
    return True
def upload_file_to_bucket(path,fileName,bucket):
    fh=MediaFileUpload(path+"\\"+fileName,  resumable=True)
    service=create_service()
    try:
        req = service.objects().insert(bucket=bucket,media_body=fh, body={'name': '1\%s' %fileName})
        response=req.execute()
    except discovery.HttpError as e:
        if (e.resp['status'] == '500' or '400'):
            return False 
    return True
         
    
def upload_dir_to_Bucket(dirPath,bucketName):
    logging.info("Uploading local dir: %s to gcs bucket: %s  " % (dirPath, bucketName))
    for dirname, dirnames, filenames in os.walk(dirPath):
        print dirname
        for filename in filenames:
            print filename
            upload_file_to_bucket(dirname,filename,bucketName)
   
    
#parse arguments
parser = argparse.ArgumentParser(description='sync dir to Google cloud sotrage')
parser.add_argument('source', type=str ,help='source directory')
parser.add_argument('bucket', type=str ,help='bucket name gcloud')

args = parser.parse_args()
logging.info(args)

#vars
projectName="valooto-production"
bucketName=args.bucket
dir_root=args.source+"\\"

#verify source dir exists:
if not os.path.isdir(dir_root):
    logging.critical ("source root_dir: %s doesn't exist, exiting" % dir_root)
    exit()
    
#get child folders (will be buckets)
localBuckets= load_dirs(dir_root)
service=create_service()

#for each local dir upload to respective bucket of the same name
for localBucket in localBuckets:
    #check if bucket exists
    if (not is_bucket(localBucket)):
        logging.critical("local folder: %s doesn't exist in gcloud storgae- create bucket with name:" % localBucket)
        continue
    else:
        result=upload_dir_to_Bucket(dir_root+localBucket,localBucket)
        print result
    
    #files=load_files(localBucket)
    
        
            




 

#pprint.pprint (buckets)
#pprint.pprint (load_dirs(dir_root))
#files=load_files(dir_root)

#for each dir under root create bucket













'''
Package Create new branch
creates a new branch from master,
naming convention: 
where :x.y.z
x - Major
y - Minor
z - Build num.
'''

def create_branch(versionNum):
    return False

def get_current_version(versionFilePath):
    import re
    import os
    '''
    reads current version file from path
    return list: [Major,Minor,Build]
    '''
    print "---get_current_version\n"
    print "Opening File:%s ..." % versionFilePath
    try:
        fileObj=open(versionFilePath,'r+')
    except IOError as e:
        print "unable to open file: %s" %e
        exit()
    fileContent=fileObj.readline()
    fileObj.close()
    #check if file is empty
    if not (os.stat(versionFilePath).st_size == 0):
        matchObj=re.match(r"(\d+).(\d+).(\d+)", fileContent)
    else:
        print "file is empty\n"
        return False
    
    return [matchObj.group(1),matchObj.group(2),matchObj.group(3)]

def set_current_version(versionFilePath,version):
    '''
    sets current version file from given list
    return:
    True - set version was successful
    False - set version failed.
    '''
   
    print "---set_current_version\n"
    print "Opening File:%s ..." % versionFilePath
    try:
        fileObj=open(versionFilePath,'w+')
    except IOError as e:
        print "unable to open file: %s" %e
        exit()
    print "writing version to file\n"
    fileObj.write(version)
    
    
    
########################################################
#
#
########################################################

import argparse

#parse arguments
parser = argparse.ArgumentParser(description='create new branch')
parser.add_argument('path', type=str ,help='path to current version file.')

args = parser.parse_args()

version = get_current_version(args.path)
print version
version[0]=1
print version
print set_current_version(args.path,version)


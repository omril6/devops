import jenkins
import datetime
import argparse


def get_server_instance():
    
    return jenkins.Jenkins(jenkins_url, username = user, password = passwd)

def job_exists(jobName):
    server=get_server_instance()
    if server.get_job_name(jobName) is None:
        return False
    else:
        return True
    
def get_jobs(jobName):
    server=get_server_instance( )
    return server.get_job_config(jobName)

def create_view(name, viewConfig):
    
    try:
        server= get_server_instance()
        server.create_view(name, viewConfig)
    except jenkins.JenkinsException as e:
        print "[ERROR] %s" % e
        return False 
    return True

def get_views():
    server=get_server_instance( )
    return server.get_views()

def create_job(jobName,jobXml):
    try:
        server=get_server_instance()
        server.create_job(jobName, jobXml)
    except jenkins.JenkinsException as e:
        print "[ERROR] %s" % e
        return False
    return True


'''
creates a new job in jenkins
'''

print "********************************"
print datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
print "Starting script jenkins new view"
print "********************************"

#parse arguments
parser = argparse.ArgumentParser(description='creates a new view in jenkins')
parser.add_argument('version', type=str ,help='name of new job')
args = parser.parse_args()

#VARS
user= "jenkins@valooto.com"
passwd="c1099d6ad3d6b7a247c07baf341df6c4"
jenkins_url = 'https://jenkins.valooto.com'
branchName=args.version
jobName="build-prod_%s" % branchName

#job template
jobXml=get_jobs("build-prod-template")

jobXml=jobXml.replace("<name>branch_here</name>","<name>%s</name>" % jobName)

if job_exists(jobName) is not True:
    try:
        create_job(jobName, jobXml)
    except jenkins.JenkinsException as e:
        print "[ERROR] %s" % e





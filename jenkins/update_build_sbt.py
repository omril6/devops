# !/usr/bin/python
'''
gets:
current version file
gets action: major/minor/build/snapshot
build.sbt path
'''
import datetime
import argparse  

def get_curr_version(path):
    '''
    gets full path to file.
    reads from file
    returns array of version [Major,Minor,build]
    '''
    print "Openning conf file: %s" % path
    
    try:
        fobj = open (path,'r')
        
    except IOError as e:
        print "IO error:%s" %e        
    version=fobj.readline()
    version=version.split(".")
    version=map(int,version)
    return version

def increment_version(version,action):
    '''
    gets version as array of [Major,Minor,build]
    gets action: major/minor/build/snapshot
    returns new version
    '''
    if action == 'major':
        ##advance major and write o to minor and build
        version[0]+= 1
        version[1]= 0
        version[2]= 0
        print "+%s version incremented" % action
    
    if action == 'minor':
        version[1]+=1
        print "+%s version incremented" % action
    
    if action == 'build':
        version[2]+=1
        print "+%s version incremented" % action
    
    if action == 'snapshot':
        print "+snapshot version - not incrementing"
    return version

def update_build_sbt(path,version,action):
    '''
    gets path to build.sbt
    gets version as array of [Major,Minor,build]
    '''
    #open build.sbt for wrtiting.
    
    try:
        fobj = open (path,'r+')
    except IOError as e:
        print "IO error:%s" %e
        quit()
    
    
    #read build.sbt and change version.
    newFile=""
    for line in fobj.readlines():
        if "version :=" in line:
            #newFile=newFile+"version := \"%d.%d.%d\""+"\n" % (version[0],version[1],version[2])
            if (action == 'snapshot'):
                newVer="\"%s.%s.%s-SNAPSHOT-%s\"" % (version[0],version[1],version[2],datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d_%H:%M:%S'))
            else:
                newVer="\"%s.%s.%s\"" % (version[0],version[1],version[2])
            
            newline="version := %s " %newVer
            newFile=newFile+newline+"\n"
        else:
            newFile=newFile+line         
    fobj.close()
    
    #write new build.sbt
    try:
        fobj = open (path,'w')
        fobj.write(newFile)
    except IOError as e:
        print "IO error:%s" %e
        quit()
    
    return True

def update_version_file(path,version):
    try:
        fobj = open (path,'w')
        fobj.write("%s.%s.%s" % (version[0],version[1],version[2]) )
    except IOError as e:
        print "IO error:%s" %e
    
    return True

#parse arguments
parser = argparse.ArgumentParser(description='updates build.sbt file for this build')
parser.add_argument('build', type=str ,help='path to built.sbt')
parser.add_argument('version', type=str , help='path to current version file')
parser.add_argument('action', type=str , help='which int to increment for this version, accepts major/minor/build/snapshot string only')
args = parser.parse_args()


#print get_curr_version(args.version)
print "********************************"
print datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
print "Starting script Update_build_sbt"
print "********************************"
currVer=get_curr_version(args.version)
nextVer=increment_version((currVer),args.action.lower())
print "+new version: %s.%s.%s" % (nextVer[0],nextVer[1],nextVer[2]) 

if not (update_build_sbt(args.build,nextVer,args.action.lower())):
    print "-failed to update build.sbt file"
    quit()
else:
    print "+build.sbt written successfully"
if not (update_version_file(args.version,nextVer)):
    print "-failed to update current_verison file"
    quit()
else:
    print "+current_version file written successfully"
print "************************"


#!/usr/bin/python
import os
import logging
import re
import fileinput


#import argparse

def get_conf_file(filename):
    print "Openning conf file: %s" % filename
    
    try:
        fobj = open (filename,'r')
    except IOError as e:
        print "IO error:%s" %e        
    return fobj.readlines()

def get_service_name(conf_file):
    regex = "^name = (.*)$"
    for line in conf_file:
        matchObj=re.match(regex, line, re.I)
        if matchObj is not None:
            return matchObj.group(1)
    return -1
        
def get_curr_ver(conf_file):
    regex = "^current_version = (\d+).(\d+).(\d+)"
    
    for line in conf_file:
        matchObj=re.match(regex, line, re.I)
        if matchObj is not None:
            version_list=[matchObj.group(1),matchObj.group(2),matchObj.group(3)]
            return version_list
        
    return -1    

def get_remove_from_build(conf_file):
    regex = "^remove from build.sbt =\s(.*)"
    
    for line in conf_file:
        matchObj=re.match(regex, line, re.I)
        if matchObj is not None:
            return matchObj.group(1)
    return -1

def get_add_to_build(conf_file):
    regex = "^add to build.sbt =\s(.*)"
    
    for line in conf_file:
        matchObj=re.match(regex, line, re.I)
        if matchObj is not None:
            return matchObj.group(1)
    return -1    
    
#######################################################

def write_build_sbt(build_file_path):
    fh = open (build_file_path,'r+')
    removeList=get_remove_from_build(conf_file_string).split(",")
    new_file=""
    removeList=[x.strip(' ') for x in removeList]
    print removeList
    addList=get_add_to_build(conf_file_string).split(",")
    addList= [x.strip(' ') for x in addList]
    print addList
    for line in fh:
        rFound=False
        
        for remove in removeList:
            if remove in line:
                print "remove found!"
                rFound=True
        
        if rFound is False:
            new_file=new_file+line+"\n"
    fh.close()
    
    print new_file+"\n========================="
    #fh = open (build_file_path,'w')
    #fh.write(new_file)
    #fh.close()
    return    

conf_fn="jenkins-valooto.conf"
conf_path="./"
#path to build.sbt
build_file_path="webserver/build.sbt"

'''
conf file will save: current version number,
service name, etc..
'''
conf_file_string = get_conf_file(conf_path+conf_fn)
#print get_curr_ver(conf_file_string)
print get_remove_from_build(conf_file_string)
print get_add_to_build(conf_file_string)
print "============================\n"
write_build_sbt(build_file_path)



print "script finished successfully."



# !/usr/bin/python
import os
import re
import argparse  

'''
finds the filename of the new package and uploads it to artifactory
'''

def get_deb_package_name(path):
    for root, dirs, files in os.walk(path):
        for name in files:
            matchObj=re.match(r"(.*).deb", name, re.I)
            if matchObj:
                return name



#parse arguments
parser = argparse.ArgumentParser(description='uploads artifact to artifactory repository')
parser.add_argument('repo', type=str ,help='name of remote repository')
args = parser.parse_args()

###
fn=get_deb_package_name(os.getcwd()+"/webserver/target")

url="\"https://valooto.jfrog.io/valooto/%s/%s;deb.distribution=jessie;deb.component=main;deb.architecture=amd64\"" % (args.repo,fn)
user="jenkins"
passwd="Jenkinspass1"
path=os.getcwd()+"/webserver/target/%s" % fn
cmd="curl -u%s:%s -XPUT %s -T %s" % (user,passwd,url,path)
print cmd
if fn is not None:
    
    output=os.popen(cmd).read()
    
else:
    print "filename not found"

print output
if "\"errors\"" in output:
    exit(1)

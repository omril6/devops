#!/usr/bin/python
#Script: File compare
#Author: Omri Levi
#
#Assumptions:
#all files in the folder are saved as binary not ASCII
import argparse
import os
import pprint
import operator

def load_files(path):
    all_files={}
    dir_list=os.listdir(path)
    for fn in dir_list:
        if (os.path.isfile(path+fn)):
            aFile=os.stat(path+fn)
            all_files[path+fn]=aFile.st_size
    return all_files

def sort_by_size(files):
    sorted_files = sorted(files.items(), key=operator.itemgetter(1))
    return sorted_files


def print_duplicates(file_list):
    for tup in file_list:
        print(tup[0])
        
    return

path="F:\\temp\\"
print ("Script Started\n")
files=load_files(path)
files_list=sort_by_size(files)
#pprint.pprint(files_list)
print_duplicates(files_list)


print ("Script Finished\n")